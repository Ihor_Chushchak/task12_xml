package com.epam;

import org.apache.commons.io.FilenameUtils;
import java.io.File;

import static com.epam.model.constants.ConstantObj.END_XML;
import static com.epam.model.constants.ConstantObj.END_XSD;

public class Checker {
    public static boolean isXML(File xml) {
        return xml.isFile() && FilenameUtils.getExtension(xml.getName()).equals(END_XML);
    }

    public static boolean isXSD(File xsd){
        return xsd.isFile() && FilenameUtils.getExtension(xsd.getName()).equals(END_XSD);
    }
}
