package com.epam.model.bank;

public class AmountOnDeposit {
    private Integer count;

    public AmountOnDeposit(){
    }

    public AmountOnDeposit(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "AmountOnDeposit{" +
                "count=" + count +
                "}\n";
    }
}
