package com.epam.model.bank;

public class Profitability {
    private Double profitability;

    public Profitability() {
    }

    public Profitability(Double profitability) {
        this.profitability = profitability;
    }

    public Double getProfitability() {
        return profitability;
    }

    public void setProfitability(Double profitability) {
        this.profitability = profitability;
    }

    @Override
    public String toString() {
        return "Profitability{" +
                "profitability=" + profitability +
                "}\n";
    }
}
