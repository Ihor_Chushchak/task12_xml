package com.epam.model.constants;

public class ConstantObj {
    public static final String END_XML = "xml";
    public static final String END_XSD = "xsd";
    public static final String EMPTY_STIRNG = "";

    private ConstantObj() {
    }
}
