package com.epam.model.constants;

public class ConstantWays {
    public static final String WAY_BANK_XML = "src\\main\\resources\\XML.xml";
    public static final String WAY_BANK_XSD = "src\\main\\resources\\XSD.xsd";

    private ConstantWays() {
    }
}
