package com.epam.parsers;

import com.epam.Checker;
import com.epam.model.Main;
import com.epam.parsers.domparses.UserDom;
import com.epam.parsers.saxparser.SAXParserUser;
import com.epam.parsers.staxparser.StAXReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;

import static com.epam.model.constants.ConstantWays.WAY_BANK_XML;
import static com.epam.model.constants.ConstantWays.WAY_BANK_XSD;

public class Parser {
    private static final Logger LOG = LogManager.getLogger(Parser.class);

    public static void main(String[] args) {
        File xml = new File(WAY_BANK_XML);
        File xsd = new File(WAY_BANK_XSD);

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            showList(SAXParserUser.parseBanks(xml, xsd), "SAX");

            showList(StAXReader.parseBanks(xml), "StAX");

            showList(UserDom.getBankList(xml), "DOM");
        }
    }

    private static boolean checkIfXSD(File xsd) {
        return Checker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return Checker.isXML(xml);
    }

    private static void showList(List<Main> banks, String parserName) {
        LOG.debug("parser: "+parserName);
        for(Main bank:banks){
            LOG.info(bank);
        }
    }

}
