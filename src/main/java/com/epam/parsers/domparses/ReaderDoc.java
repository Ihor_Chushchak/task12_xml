package com.epam.parsers.domparses;

import com.epam.model.Main;
import com.epam.model.bank.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class ReaderDoc {
    public List<Main> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Main> banks = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("bank");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Main bank = new Main();

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;
                bank.setBankNum(Integer.parseInt(element.getAttribute("bankNum")));
                bank.setName(new Name(element.getElementsByTagName("name").item(0).getTextContent()));
                bank.setCountry(new Country(element.getElementsByTagName("country").item(0).getTextContent()));
                bank.setType(new Type(element.getElementsByTagName("type").item(0).getTextContent()));
                bank.setMyDepositor(new Depositor(element.getElementsByTagName("depositor").item(0).getTextContent()));
                bank.setAccountID(new AccountID(element.getElementsByTagName("accountID").item(0).getTextContent()));
                bank.setProfitability(new Profitability(Double.parseDouble(element.getElementsByTagName("profitability").item(0).getTextContent())));
                bank.setAmountOnDeposit(new AmountOnDeposit(Integer.parseInt(element.getElementsByTagName("amountOnDeposit").item(0).getTextContent())));
                bank.setConstraints(new Constraints());
                bank.setConstraints(getConstraints(element.getElementsByTagName("constraints")));
                banks.add(bank);
            }
        }
        return banks;
    }
    private Constraints getConstraints(NodeList nodes){
        Constraints constraints = new Constraints();
        if(nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element)nodes.item(0);
            constraints.setAmount(Double.parseDouble(element.getElementsByTagName("timec").item(0).getTextContent()));
            constraints.setType(element.getElementsByTagName("typec").item(0).getTextContent());
        }
        return constraints;
    }
}