package com.epam.parsers.domparses;

import com.epam.model.Main;
import org.w3c.dom.Document;

import java.io.File;
import java.util.List;

public class UserDom {
    public static List<Main> getBankList(File xml){
        DomDoc creator = new DomDoc(xml);
        Document doc = creator.getDocument();
        ReaderDoc reader = new ReaderDoc();
        return reader.readDoc(doc);
    }
}
