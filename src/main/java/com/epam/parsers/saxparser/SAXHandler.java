package com.epam.parsers.saxparser;

import com.epam.model.Main;
import com.epam.model.bank.*;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.*;


import java.util.*;

public class SAXHandler extends DefaultHandler {
    private List<Main> bankList;
    private Main bank = null;
    private Constraints constraints;
    private Map<String, Boolean> elements;

    public SAXHandler() {
        super();
        bankList = new ArrayList<>();
        initAttributes();
    }

    private void initAttributes() {
        elements = new LinkedHashMap<>();

        elements.put("bankNum", false);
        elements.put("name", false);
        elements.put("country", false);
        elements.put("type", false);
        elements.put("depositor", false);
        elements.put("accountID", false);
        elements.put("profitability", false);
        elements.put("amountOnDeposit", false);
        elements.put("constraints", false);
        elements.put("timec", false);
        elements.put("typec", false);
    }

    public List<Main> getBankList() {
        return this.bankList;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (elements.keySet().contains(qName)) {
            elements.put(qName, true);
        } else if (qName.equalsIgnoreCase("bank")) {
            String bankNum = attributes.getValue("bankNum");
            bank = new Main();
            bank.setBankNum(Integer.parseInt(bankNum));
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("bank")) {
            bankList.add(bank);
        }
    }

    private void setElementToFalse(String keyElement) {
        elements.put(keyElement, false);
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (elements.get("name")) {
            bank.setName(new Name(new String(ch, start, length)));
            setElementToFalse("name");
        } else if (elements.get("country")) {
            bank.setCountry(new Country(new String(ch, start, length)));
            setElementToFalse("country");
        } else if (elements.get("type")) {
            bank.setType(new Type(new String(ch, start, length)));
            setElementToFalse("type");
        } else if (elements.get("depositor")) {
            bank.setMyDepositor(new Depositor(new String(ch, start, length)));
            setElementToFalse("depositor");
        } else if (elements.get("accountID")) {
            bank.setAccountID(new AccountID(new String(ch, start, length)));
            setElementToFalse("accountID");
        } else if (elements.get("profitability")) {
            bank.setProfitability(new Profitability(Double.parseDouble(new String(ch, start, length))));
            setElementToFalse("profitability");
        } else if (elements.get("amountOnDeposit")) {
            bank.setAmountOnDeposit(new AmountOnDeposit(Integer.parseInt(new String(ch, start, length))));
            setElementToFalse("amountOnDeposit");
        } else if (elements.get("constraints")) {
            constraints = new Constraints();
            setElementToFalse("constraints");
        } else if (elements.get("timec")) {
            constraints.setAmount(Double.parseDouble(new String(ch, start, length)));
            setElementToFalse("timec");
        } else if (elements.get("typec")) {
            constraints.setType(new String(ch, start, length));
            bank.setConstraints(constraints);
            setElementToFalse("typec");
        }
    }
}