package com.epam.parsers.saxparser;

import com.epam.model.Main;
import org.xml.sax.SAXException;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SAXParserUser {
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

    public static List<Main> parseBanks(File xml, File xsd){
        List<Main> bankList = new ArrayList<>();
        try {
            saxParserFactory.setSchema(SAXValidator.createSchema(xsd));

            SAXParser saxParser = saxParserFactory.newSAXParser();
            SAXHandler saxHandler = new SAXHandler();
            saxParser.parse(xml, saxHandler);

            bankList = saxHandler.getBankList();
        }catch (SAXException | ParserConfigurationException | IOException ex){
            ex.printStackTrace();
        }
        return bankList;
    }
}
