package com.epam.parsers.staxparser;

import com.epam.model.Main;
import com.epam.model.bank.*;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StAXReader {
    public static List<Main> parseBanks(File xml){
        List<Main> beerList = new ArrayList<>();
        Main bank = null;
        Constraints constraints = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "bank":
                            bank = new Main();

                            Attribute idAttr = startElement.getAttributeByName(new QName("bankNum"));
                            if (idAttr != null) {
                                bank.setBankNum(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setName(new Name(xmlEvent.asCharacters().getData()));
                            break;
                        case "country":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setCountry(new Country(xmlEvent.asCharacters().getData()));
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setType(new Type(xmlEvent.asCharacters().getData()));
                            break;
                        case "depositor":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setMyDepositor(new Depositor(xmlEvent.asCharacters().getData()));
                            break;
                        case "accountID":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setAccountID(new AccountID(xmlEvent.asCharacters().getData()));
                            break;
                        case "profitability":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setProfitability(new Profitability(Double.parseDouble(xmlEvent.asCharacters().getData())));
                            break;
                        case "amountOnDeposit":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setAmountOnDeposit(new AmountOnDeposit(Integer.parseInt(xmlEvent.asCharacters().getData())));
                            break;
                        case "constraints":
                            xmlEvent = xmlEventReader.nextEvent();
                            constraints = new Constraints();
                            break;
                        case "timec":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(constraints).setAmount(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "typec":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(constraints).setType(xmlEvent.asCharacters().getData());
                            Objects.requireNonNull(bank).setConstraints(constraints);
                            break;
                    }
                }
                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("bank")){
                        beerList.add(bank);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return beerList;
    }
}
