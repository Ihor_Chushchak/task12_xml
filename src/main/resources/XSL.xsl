<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <h2>Bank info</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Name</th>
                        <th>Country</th>
                        <th>Type</th>
                        <th>Depositor</th>
                        <th>AccountID</th>
                        <th>Profitability</th>
                        <th>Amount on deposit</th>
                        <th>Time constraints</th>
                        <th>Type constraints</th>
                    </tr>
                    <xsl:for-each select="banks/bank">
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="country"/></td>
                            <td><xsl:value-of select="type"/></td>
                            <td><xsl:value-of select="depositor"/></td>
                            <td><xsl:value-of select="accountID"/></td>
                            <td><xsl:value-of select="profitability"/></td>
                            <td><xsl:value-of select="amountOnDeposit"/></td>
                            <td><xsl:value-of select="constraints/timec"/></td>
                            <td><xsl:value-of select="constraints/typec"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>